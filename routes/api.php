<?php

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\HelpController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get('timezone', [HelpController::class, 'zoneTime']);

Route::group(
    ['middleware' => 'auth.api'],
    function () {
        Route::post('register', [AuthController::class, 'register']);
        Route::post('login', [AuthController::class, 'login']);
        Route::post('logout', [AuthController::class, 'logout']);

        Route::group(
            [
                'middleware' => 'jwt.verify',
                "prefix" => 'v1'
            ],
            function () {
                Route::get('me', [AuthController::class, 'me']);
                Route::prefix('categoria')->group(function () {
                    Route::get('/', [CategoriesController::class, 'index']);
                    Route::post('create', [CategoriesController::class, 'store']);
                    Route::put('update/{id}', [CategoriesController::class, 'update']);
                });
            }
        );
    }
);
