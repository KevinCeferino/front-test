<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\UserLoginRequest;
use App\Http\Requests\AuthRequest;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Http;

class AuthController extends Controller
{
    public function register(AuthRequest $request)
    {

        try {
            if ($request->get('type_user_id') == 1) {
                // app(UserJuristicRequest::class);
                $user = $this->createUserJuristic($request);
            } elseif ($request->get('type_user_id') == 2) {
                // app(UserNativeRequest::class);
                $user = $this->createUserNative($request);
            }
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'Ha ocurrido un error, si el error persiste comuniquese.',
                    // 'err' => $e->getMessage()
                ]
            );
        }

        $token = JWTAuth::fromUser($user);

        return response()->json(
            [
                'user' => $user,
                'token' => $token
            ],
            HttpResponse::HTTP_CREATED
        );
    }

    public function login(UserLoginRequest $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if (!$token = auth()->attempt($credentials)) {
                return response()->json(['error' => 'Unauthorized'], 401);
            }
        } catch (JWTException $e) {
            return response()->json([
                'error' => 'Not created token'
            ], 500);
        }

        return $this->respondWithToken($token);
    }

    public function me(Request $request)
    {
        try {
            return response()->json(auth()->user());
        } catch (Exception $e) {
            return response()->json(['error' => 'Ha ocurrido un problema, si el error persiste, comuniquese.']);
        }
    }

    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Cierre de sesión exitoso'], HttpResponse::HTTP_OK);
    }
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 1
        ], HttpResponse::HTTP_ACCEPTED);
    }

    private function createUserNative(Request $request)
    {

        return User::create(
            [
                'name' => $request->get('name'),
                'lastname' => $request->get('lastname'),
                'telephone' => $request->get('telephone'),
                'identy_document' => $request->get('identy_document'),
                'type_user_id' => $request->get('type_user_id'),
                'password' => bcrypt($request->get('password')),
                'email' => $request->get('email'),
                'verify_tc' => 0,
                'profile_photo_url' => 'https://ui-avatars.com/api/?name=&color=7F9CF5&background=EBF4FF'
            ]
        );
    }

    private function createUserJuristic(Request $request)
    {

        return User::create(
            [
                'telephone' => $request->get('telephone'),
                'NIT' => $request->get('NIT'),
                'razon_social' => $request->get('razon_social'),
                'type_user_id' => $request->get('type_user_id'),
                'password' => bcrypt($request->get('password')),
                'email' => $request->get('email'),
                'verify_tc' => 0,
                'profile_photo_url' => 'https://ui-avatars.com/api/?name=&color=7F9CF5&background=EBF4FF'
            ]
        );
    }
}
