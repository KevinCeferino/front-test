<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use Exception;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $categories = Categories::all();
            return response()->json(["categorias" => $categories], HttpResponse::HTTP_OK);
        } catch (Exception $e) {
            return response()->json(['error' => 'Ha ocurrido un problema, si el error persiste, comuniquese'], HttpResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request, ['nombre' => 'required|max:255']);
        try {
            $category = Categories::create([
                'nombre' => $request->get('nombre')
            ]);
            return response()->json(['categoria' => $category], HttpResponse::HTTP_CREATED);
        } catch (Exception $e) {
            return response()->json(['error' => 'Ha ocurrido un problema, si el error persiste, comuniquese'], HttpResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $this->validate($request, ['nombre' => 'required|max:255']);

        try {
            $category = Categories::find($id);
            $category->nombre = $request->get('nombre');
            $category->save();
            return response()->json(['categoria' => $category], HttpResponse::HTTP_OK);
        } catch (Exception $e) {
            return response()->json(['error' => 'Ha ocurrido un problema, si el error persiste, comuniquese'], HttpResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
