<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Exception;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Http\Request;

class HelpController extends Controller
{
    public function zoneTime()
    {
        try {

            $dateNow = Carbon::now('America/Bogota')->format("Y-m-d\TH:i:s\Z");
            $responseObject = (object) array('timezone' => $dateNow);

            return response()->json($responseObject, HttpResponse::HTTP_OK);
        } catch (Exception $e) {
            return response()->json(
                [
                    'response' => 'Ah ocurrido un error interno, si el problema persiste comunicate con un administrador!'
                ],
                HttpResponse::HTTP_NOT_FOUND
            );
        }
    }
}
