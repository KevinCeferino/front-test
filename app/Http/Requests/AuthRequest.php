<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $type = $this->get('type_user_id');
        $auth = [
            'telephone' => 'required|numeric|unique:users',
            'type_user_id' => 'required|numeric|max:2',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'verify_tc' => 'required|boolean',
        ];

        $juristic = [
            'NIT' => 'required|numeric|max:999999999|unique:users,NIT',
            'razon_social' => 'required|string|max:255|unique:users,razon_social',
        ];

        $native = [
            'name' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'identy_document' => 'required|integer|unique:users,identy_document'
        ];

        return $type == 1 ? array_merge($auth, $juristic) : ($type == 2 ? array_merge($auth, $native) : '');
    }

    /**
     * Method messages
     *
     * @return array with message to error
     */
    public function messages()
    {
        return [
            'telephone.required' => 'El numero de teléfono es requerido',
            'telephone.numeric' => 'El numero de teléfono es invalido',
            'telephone.max' => 'El numero de teléfono excede la longitud maxima',
            'telephone.unique' => 'El numero de teléfono ya esta registrado',

            'type_user_id.required' => 'El tipo de usuario es requerido',
            'type_user_id.numeric' => 'El tipo de usuario es invalido',
            'type_user_id.max' => 'El tipo de usuario excede la longitud maxima',

            'email.required' => 'El correo electronico es requerido',
            'email.string' => 'El correo electronico es invalido',
            'email.email' => 'El correo electronico es invalido',
            'email.max' => 'El correo electronico excede la longitud maxima',
            'email.unique' => 'El correo electronico ya esta registrado',

            'password.required' => 'La contraseña es requerida',
            'password.string' => 'La contraseña es invalida',
            'password.min' => 'La contraseña necesita una longitud minima de :min',
            'password.confirmed' => 'Las contraseñas no coinciden',

            'verify_tc.required' => 'Debe aceptar los terminos y condiciones',
            'verify_tc.boolean' => 'Ocurrio un error con la verificación',

            'name.required' => 'El nombre es requerido',
            'name.string' => 'El nombre es invalido',
            'name.max' => 'El nombre excede la longitud maxima',

            'lastname.required' => 'El apellido es requerido',
            'lastname.string' => 'El apellido es invalido',
            'lastname.max' => 'El apellido excede la longitud maxima',

            'identy_document.required' => 'El documento de identidad es requerido',
            'identy_document.integer' => 'El documento de identidad es invalido',
            'identy_document.max' => 'El documento de identidad excede la longitud maxima',
            'identy_document.unique' => 'El documento de identidad ya esta registrado',

            'NIT.required' => 'El NIT es requerido',
            'NIT.numeric' => 'El NIT es invalidom solo recibe numeros',
            'NIT.max' => 'El NIT excede la longitud maxima',
            'NIT.unique' => 'El NIT ya esta registrado',

            'razon_social.required' => 'La Razon Social es requerida',
            'razon_social.string' => 'La Razon Social es invalida',
            'razon_social.max' => 'La Razon Social excede la longitud maxima',
            'razon_social.unique' => 'La Razon Social ya esta registrada',
        ];
    }
}
