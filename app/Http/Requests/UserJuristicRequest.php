<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserJuristicRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'NIT' => 'required|numeric|max:999999999|unique:users,NIT',
            'razon_social' => 'required|string|max:255|unique:users,razon_social',
        ];
    }

    public function messages()
    {
        return [
            'NIT.required' => 'El NIT es requerido',
            'NIT.numeric' => 'El NIT es invalidom solo recibe numeros',
            'NIT.max' => 'El NIT excede la longitud maxima',
            'NIT.unique' => 'El NIT ya esta registrado',

            'razon_social.required' => 'La Razon Social es requerida',
            'razon_social.string' => 'La Razon Social es invalida',
            'razon_social.max' => 'La Razon Social excede la longitud maxima',
            'razon_social.unique' => 'La Razon Social ya esta registrada',
        ];
    }
}
