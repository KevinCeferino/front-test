<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class UserRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6'
        ];
    }

    public function messages()
    {
        return [
            'name.required'   => 'Nombre es requerido',
            'name.max'   => 'El nombre es demasiado largo',
            'email.unique' => 'El correo electrónico ya existe',
            'email.email' => "El correo electronico no es válido",
            'email.max' => 'El correo electronico es demasiado largo',
            'email.required' => 'El correo electronico es necesario',
            'password.required' => 'La contraseña es necesaria',
            'password.min' => 'La contraseña debe ser mayor a 6 digitos',
            'telephone.min'   => 'El telefono requiere minimo 6 numeros',
            'telephone.unique' => 'El telefono que esta ingresando ya esta registrado',
            'code_area_id.required'   => 'Codigo de area es requerido',
            'identy_document.required'   => 'La cedula es requerida',
            'NIT.required'   => 'El nit  es requerido',
            'razon_social.required'   => 'Nombre es requerido',
            // 'verify_tc.required'   => 'Acepte los terminos y condiciones',
            'identy_document.required_without'   => 'El campo del documento de identidad es obligatorio cuando nit no está presente.',
            'NIT.required_without'   => 'El campo del documento de identidad es obligatorio cuando cedula no está presente.',
            'name.required_without'   => 'El campo de nombre es obligatorio cuando NIT no está presente.'
        ];
    }
}
