<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserNativeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'identy_document' => 'required|integer|unique:users,identy_document'
        ];
    }

    /**
     * Method messages
     *
     * @return array with message to error
     */
    public function messages()
    {
        return [
            'name.required' => 'El nombre es requerido',
            'name.string' => 'El nombre es invalido',
            'name.max' => 'El nombre excede la longitud maxima',

            'lastname.required' => 'El apellido es requerido',
            'lastname.string' => 'El apellido es invalido',
            'lastname.max' => 'El apellido excede la longitud maxima',

            'identy_document.required' => 'El documento de identidad es requerido',
            'identy_document.integer' => 'El documento de identidad es invalido',
            'identy_document.max' => 'El documento de identidad excede la longitud maxima',
            'identy_document.unique' => 'El documento de identidad ya esta registrado',
        ];
    }
}
