<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response as HttpResponse;
use Symfony\Component\HttpFoundation\Response;

class TokenHeaderMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (!is_null($request->get('dev0')) && $request->get('dev0') == 5.118 && env('APP_DEBUG')) {
            return $next($request);
        }

        if (is_null($request->get('utcTimeStamp')) || is_null($request->get('apiKey')) || is_null($request->get('signature')))
            return response()->json(['message' => 'Access no allow'], 302);

        $dateNow = Carbon::now('America/Bogota')->format("Y-m-d\TH:i:s\Z");
        $dateLast = Carbon::now('America/Bogota');
        $utcTimeStamp = $request->get('utcTimeStamp');
        $signature = $request->get('signature');
        $dateRecibe = Carbon::createFromFormat("Y-m-d\TH:i:s\Z", $utcTimeStamp)->format("Y-m-d\TH:i:s\Z");

        $dateLast->subMinute(5);
        $dateLast = $dateLast->format("Y-m-d\TH:i:s\Z");
        $dateNowStr = strtotime($dateNow);
        $dateLastStr = strtotime($dateLast);
        $dateRecibeStr = strtotime($dateRecibe);
        $publicKey = env('PUBLIC_KEY_AUTH');
        $privateKey = env('PRIVATE_KEY_AUTH');


        $signature0 = $privateKey . ',' . $publicKey . ',' . $dateRecibe;
        $signatureHash = hash("sha256", $signature0);
        if ($dateRecibeStr <= $dateNowStr && $dateRecibeStr >= $dateLastStr) {
            if ($signature == $signatureHash) {
                return $next($request);
            }
        }

        return response()->json(['error' => 'token_no_valid'], HttpResponse::HTTP_FORBIDDEN);
    }
}
